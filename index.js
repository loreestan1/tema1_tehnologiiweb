
const FIRST_NAME = "Lorena";
const LAST_NAME = "Stan";
const GRUPA = "1083";

/**
 * Make the implementation here
 */

function numberParser(value)
{
    if(value > Number.MAX_SAFE_INTEGER || value < Number.MIN_SAFE_INTEGER)
     return NaN;
     
    return parseInt(value);
}

function dynamicPropertyChecker(input, property) {
    
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

